# Stage 1: Build the Next.js application
FROM node:18-alpine as builder

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json (or yarn.lock)
COPY package*.json ./
COPY yarn.lock ./

# Install dependencies
RUN yarn install --frozen-lockfile

# Copy the rest of your Next.js application source code
COPY . .

# Build your Next.js application
RUN yarn build

# Stage 2: Run the application
FROM node:18-alpine

WORKDIR /app

# Copy the build output from the builder stage
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/public ./public
COPY --from=builder /app/package.json ./package.json

# Install production dependencies only
RUN yarn install --production --frozen-lockcheck && yarn cache clean

# Expose the port Next.js runs on
EXPOSE 3000

# Command to run your app
CMD ["yarn", "start"]
