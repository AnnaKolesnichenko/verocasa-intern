import { FeatureCollection, GeoJsonProperties, Point } from 'geojson';

export const createGeoJSONMapPage = (dataMarker: any): FeatureCollection<Point, GeoJsonProperties> => {
  return {
    type: 'FeatureCollection',
    features: dataMarker.map((item: any) => ({
      type: 'Feature',
      properties: {
        id: item.projectId,
        photo: item.picture,
        name: item.name,
        price: item.price_from
      },
      geometry: {
        type: 'Point',
        coordinates: [item.coordinates?.lng, item.coordinates?.lat]
      }
    }))
  };
};
