import { PriceType } from '@/store/filterStore';

export const handlePriceChange = (
  value: number,
  selectValue: PriceType,
  action: (values: PriceType) => void
) => {
  // Копіюємо поточні значення selectValue для подальших змін
  let newValues: PriceType = { min: selectValue.min, max: selectValue.max };

  // Якщо selectValue порожній додаємо min
  if (newValues.min === null && newValues.max === null) {
    newValues.min = value;
    //Якщо max === null
  } else if (newValues.min !== null && newValues.max === null) {
    if (value < newValues.min) {
      newValues.max = newValues.min;
      newValues.min = value;
    } else {
      newValues.max = value;
    }
  }
  // Якщо в selectValue вже є два значення
  else {
    newValues.min = value;
    newValues.max = null;
    // Визначаємо до якого з поточних значень нове значення ближче
    /*  else if (newValues.min !== null && newValues.max !== null) {
    const minDiff = Math.abs(value - newValues.min);
    const maxDiff = Math.abs(value - newValues.max);
    if (minDiff < maxDiff) {
      newValues.min = value;
    } else {
      newValues.max = value;
    }
    */
  }

  // Викликаємо action з оновленими значеннями
  action(newValues);
};

// Введення даних в інпут
export const handleInputChange = (
  value: string, // Приймаємо значення як рядок, щоб обробляти коми
  type: 'min' | 'max',
  selectValue: PriceType,
  action: (values: PriceType) => void
) => {
  // Копіюємо поточні значення selectValue для подальших змін
  let newValues = { ...selectValue };

  // Якщо значення порожнє, встановлюємо його як null
  if (value === '') {
    if (type === 'min') {
      newValues.min = null;
    } else if (type === 'max') {
      newValues.max = null;
    }
  } else {
    // Видаляємо коми з рядка та перетворюємо його на число
    const numericValue = parseInt(value.replace(/,/g, ''), 10);

    // Якщо значення некоректне (не число або менше 0), встановлюємо його як null
    if (isNaN(numericValue) || numericValue <= 0) {
      if (type === 'min') {
        newValues.min = null;
      } else if (type === 'max') {
        newValues.max = null;
      }
    } else {
      // Обробка введення для типу 'min'
      if (type === 'min') {
        newValues.min = numericValue;
      }
      // Обробка введення для типу 'max'
      else if (type === 'max') {
        newValues.max = numericValue;
      }
    }
  }

  // Викликаємо action з оновленими значеннями
  action(newValues);
};

export const clearValue = (
  value: number,
  selectValue: PriceType,
  action: (values: PriceType) => void
) => {
  let newValue = selectValue;
  if (value === selectValue.min) {
    newValue.min = null;
  } else {
    newValue.max = null;
  }
  action(newValue);
};

// Функція для форматування чисел з комами
export const formatNumber = (value: number): any => {
  if (value) {
    return value.toLocaleString('en-US');
  } else {
    return '0';
  }
};

//фільтри для тесту
interface FilterMapParamsType {
  selectedType: string[];
  selectedBeds: string[];
  selectedPrice: PriceType;
  selectedSize: PriceType;
  selectedArea: string[];
}

export const filterMapData = (data: any, filters: FilterMapParamsType) => {
  return data.filter((item: any) => {
    const { selectedType, selectedBeds, selectedPrice, selectedSize, selectedArea } = filters;

    // Фільтрація за типом
    if (selectedType.length > 0 && !selectedType.includes(item.short_description.type)) {
      return false;
    }

    // Фільтрація за кількістю кімнат
    if (
      selectedBeds.length > 0 &&
      item.short_description.rooms !== undefined &&
      !selectedBeds.includes(item.short_description.rooms.toString())
    ) {
      return false;
    }

    // Фільтрація за ціною
    if (selectedPrice.min !== null && item.price_from < selectedPrice.min) {
      return false;
    }
    if (selectedPrice.max !== null && item.price_from > selectedPrice.max) {
      return false;
    }

    // Фільтрація за розміром
    if (selectedSize.min !== null && item.size_m2 < selectedSize.min) {
      return false;
    }
    if (selectedSize.max !== null && item.size_m2 > selectedSize.max) {
      return false;
    }

    // Фільтрація за областю
    if (selectedArea.length > 0 && !selectedArea.includes(item.area)) {
      return false;
    }

    // Якщо жодна з умов не відхилена, повертаємо true
    return true;
  });
};
