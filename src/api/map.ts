import axios from 'axios';
const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_API;

export interface SearchParamsType {
  searchParams?: string;
  selectedRadio?: string;
  selectedType?: string[];
  selectedBeds?: string[];
  selectedPrice?: { min: number | null; max: number | null };
  selectedSize?: { min: number | null; max: number | null };
  selectedArea?: string[];
  polygonData?: GeoJSON.Polygon | null;
}

export const getFilteredProjectsToMap = async (params: SearchParamsType): Promise<any> => {
  try {
    const urlParams = new URLSearchParams();
    // Додаємо параметри до URLSearchParams
    if (params.searchParams) urlParams.append('name', params.searchParams);
    if (params.selectedType && params.selectedType.length > 0) {
      params.selectedType.forEach(type => urlParams.append('type', type));
    }
    if (params.selectedBeds && params.selectedBeds.length > 0) {
      params.selectedBeds.forEach(bed => urlParams.append('beds', bed));
    }
    if (params.selectedPrice) {
      if (params.selectedPrice.min !== null)
        urlParams.append('minPrice', params.selectedPrice.min.toString());
      if (params.selectedPrice.max !== null)
        urlParams.append('maxPrice', params.selectedPrice.max.toString());
    }
    if (params.selectedSize) {
      if (params.selectedSize.min !== null)
        urlParams.append('minSize', params.selectedSize.min.toString());
      if (params.selectedSize.max !== null)
        urlParams.append('maxSize', params.selectedSize.max.toString());
    }
    if (params.selectedArea && params.selectedArea.length > 0) {
      params.selectedArea.forEach(area => urlParams.append('area', area));
    }
    // Додаємо параметри для координат полігону, якщо вони є
    if (params.polygonData?.coordinates && params.polygonData.coordinates.length > 0) {
      const polygonString = params.polygonData.coordinates.map(coord => coord.join(',')).join(';');
      urlParams.append('polygon', polygonString);
    }
   
    const queryString = urlParams.toString();
    const url = `${baseUrl}project/map/filter?${queryString}`;

    const response = await axios.get(url);
    return response;
  } catch (error) {
    console.error(`Failed to fetch data from server: ${error}`);
  }
};


 /*
    // додаємо параметри для координат полігону, якщо вони є
  if (params.polygonData?.coordinates && params.polygonData.coordinates.length > 0) {
    const polygonString: any = params.polygonData.coordinates
      .map(coord => coord.join(','))
      .join(';');
    filteredParams.polygon = polygonString;
  }
    */