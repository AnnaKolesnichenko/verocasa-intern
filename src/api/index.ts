const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_API;

//отримання списку всіх проектів для компонента search
//реалізувати отримання цих даних вперше, а потім підтягувати їх з store
export const getProjectsList = async () => {
  try{
    const response = await fetch(`${baseUrl}/project`);
    if (!response.ok) {
      console.error('Failed to fetch data');
    }
    return response.json();
  }catch(error){
    console.error(error)
  }
  
};

//отримання проектів на головній сторінці
export const getProjectsMain = async () => {
  try {
    const response = await fetch(`${baseUrl}project?page=1&size=12`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      // cache: 'no-cache', // Додаємо опцію, щоб уникнути кешування
    });
    if (!response.ok) {
      console.error('Failed to fetch');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};
//отримання всіх топових проектів

export const getTopProjects = async () => {
  try {
    const response = await fetch(`${baseUrl}project/top`);
    if (!response.ok) {
      console.error('Failed to fetch data');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};

//отримання всіх проктів для карти
export const getAllProjectsToMapMain = async () => {
  try {
    const response = await fetch(`${baseUrl}project/map`);
    if (!response.ok) {
      console.error('Failed to fetch');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};

//отримання окремого проекту за id
export const getProjectById = async (id: any) => {
  const url = `${baseUrl}project/${id}`;
  //console.log('Fetching project with URL:', url);
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Failed to fetch data: ${response.status} ${response.statusText}`);
    }
    return response.json();
  } catch (err: any) {
    console.error('Error fetching project:', err.message);
    throw err;
  }
};

export const getProjectsOthers = async () => {
  try {
    const response = await fetch(`${baseUrl}project?page=1&size=5`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      // cache: 'no-cache', // Додаємо опцію, щоб уникнути кешування
    });
    if (!response.ok) {
      console.error('Failed to fetch');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};
