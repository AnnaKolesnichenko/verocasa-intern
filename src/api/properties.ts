import axios from 'axios';
const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_API;

export interface SearchParamsType {
  sortField?: string;
  sortOrder?: string;
  searchParams?: string;
  selectedRadio?: string;
  selectedType?: string[];
  selectedBeds?: string[];
  selectedPrice?: { min: number | null; max: number | null };
  selectedSize?: { min: number | null; max: number | null };
  selectedArea?: string[];
  page?: number;
  size?: number;
}

export const getFilteredProjectsToProperties = async (params: SearchParamsType): Promise<any> => {
  try {
    const urlParams = new URLSearchParams();

    // Встановлюємо порожні параметри
    urlParams.set('name', '');
    urlParams.set('offer', '');
    urlParams.set('beds', '');
    urlParams.set('minPrice', '');
    urlParams.set('maxPrice', '');
    urlParams.set('minSize', '');
    urlParams.set('maxSize', '');
    urlParams.set('type', '');
    urlParams.set('area', '');
    urlParams.set('page', '');
    urlParams.set('size', '');
    urlParams.set('sortField', '');
    urlParams.set('sortOrder', '');

    // Оновлюємо параметри, якщо значення присутні
    if (params.searchParams) urlParams.set('name', params.searchParams);
    if (params.selectedType && params.selectedType.length > 0) {
      urlParams.set('type', params.selectedType.join(','));
    }
    if (params.selectedBeds && params.selectedBeds.length > 0) {
      urlParams.set('beds', params.selectedBeds.join(','));
    }
    if (params.selectedPrice) {
      if (params.selectedPrice.min !== null)
        urlParams.set('minPrice', params.selectedPrice.min.toString());
      if (params.selectedPrice.max !== null)
        urlParams.set('maxPrice', params.selectedPrice.max.toString());
    }
    if (params.selectedSize) {
      if (params.selectedSize.min !== null)
        urlParams.set('minSize', params.selectedSize.min.toString());
      if (params.selectedSize.max !== null)
        urlParams.set('maxSize', params.selectedSize.max.toString());
    }
    if (params.selectedArea && params.selectedArea.length > 0) {
      urlParams.set('area', params.selectedArea.join(','));
    }
    if (params.page !== undefined) urlParams.set('page', params.page.toString());
    if (params.size !== undefined) urlParams.set('size', params.size.toString());
    if (params.sortField) urlParams.set('sortField', params.sortField);
    if (params.sortOrder) urlParams.set('sortOrder', params.sortOrder);
    const queryParams = urlParams.toString();

    const url = `${baseUrl}project/filter?${queryParams}`;
  //console.log(url);
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    console.error('Failed to fetch data from server');
  }
};
