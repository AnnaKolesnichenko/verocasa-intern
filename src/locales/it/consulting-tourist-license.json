{
  "pageTitle": "Licenza Turistica",
  "pageDescription": "Questo contenuto contiene informazioni in tempo reale sui servizi legali in Spagna",

  "section_description": "Una licenza turistica in Spagna è un documento chiave per coloro che desiderano affittare la loro proprietà a turisti per brevi periodi. Avere tale licenza non solo legalizza l'attività del proprietario davanti alla legge, ma evita anche possibili multe per affitti illegali. In questo testo esamineremo quattro argomenti principali legati alla licenza turistica in Spagna: il processo di ottenimento della licenza, i documenti richiesti, gli obblighi del proprietario e le possibili multe per violazione della legge.",

  "obtaining_title": "Il processo di ottenimento di una licenza turistica",
  "obtaining_text": "Ottenere una licenza turistica in Spagna inizia con una domanda all'autorità locale responsabile del turismo nella regione in cui si trova la proprietà. Il processo può variare da una comunità autonoma all'altra, poiché ogni regione ha le proprie regole e normative riguardanti gli affitti a breve termine. Ad esempio, a Barcellona e nelle Isole Baleari, le regole possono essere più severe a causa dell'alto flusso di turisti.",

  "before_applying": "Prima di presentare la domanda, è importante assicurarsi che la proprietà soddisfi tutti i requisiti per un affitto a breve termine. Questo include verificare che l'edificio rispetti gli standard locali di sicurezza e comfort. Successivamente, è necessario raccogliere tutti i documenti necessari e presentarli all'autorità competente. Nella maggior parte dei casi, la domanda è seguita da un'ispezione della proprietà per verificare che rispetti tutti gli standard. Ad esempio, a Barcellona e nelle Isole Baleari, le regole possono essere più severe a causa dell'alto flusso di turisti.",

  "required_title": "Documenti richiesti per ottenere una licenza",
  "required_text": "Per ottenere una licenza turistica sono necessari diversi documenti, che possono variare da regione a regione. Tuttavia, l'elenco di solito include:",
  "required_list": [
    "Un modulo di domanda di licenza compilato in conformità con i requisiti del governo locale.",
    "Documenti che confermano il titolo di proprietà.",
    "Piani dell'edificio che mostrano che l'edificio è conforme alle norme di sicurezza e antincendio vigenti.",
    "Un certificato di conformità della proprietà agli standard locali di comfort e convenienza.",
    "In alcuni casi, potrebbe essere richiesto il consenso dei proprietari degli appartamenti o delle case vicine."
  ],

  "lendlors_title": "Responsabilità del proprietario",
  "lendlors_text": "Dopo aver ottenuto una licenza turistica, il proprietario deve rispettare una serie di regole e obblighi, che includono:",
  "lendlors_list": [
    "Registrazione di tutti i turisti presso la polizia locale o altra autorità competente.",
    "Rispetto dei livelli di rumore e di altre normative locali che possono essere imposte per proteggere i diritti degli altri residenti.",
    "Mantenere la proprietà in condizioni che rispettino gli standard specificati nella licenza.",
    "Fornire tutti i servizi e le comodità pubblicizzati che sono stati indicati nell'annuncio o nel contratto di locazione."
  ],

  "posible_title": "Possibili multe per violazione della legislazione",
  "posible_text": "Il mancato rispetto dei requisiti della licenza può comportare multe significative. A seconda della regione e della gravità della violazione, le multe possono variare da alcune centinaia a migliaia di euro. Le violazioni più gravi includono l'affitto di alloggi senza licenza, il mancato rapporto dei turisti alla polizia e la violazione delle norme di sicurezza antincendio.",

  "conclusion_title": "Conclusione",
  "conclusion_text": "Una licenza turistica in Spagna non è solo una necessità legale per le proprietà a breve termine, ma anche un meccanismo per garantire la sicurezza e il comfort sia degli inquilini che dei proprietari stessi. Ottenere e rispettare i termini della licenza richiede attenzione ai dettagli e responsabilità da parte del proprietario della proprietà, ma contribuisce infine a mantenere un alto livello di servizi turistici e a garantire un'esperienza positiva per turisti e residenti locali.",

  "tourist_license": "Licenza Turistica",

  "important_title": "Aspetti Importanti:",
  "important_list": [
    "Obbligatoria per gli affitti a breve termine.",
    "Formalizzazione da 60 a 120 giorni.",
    "Numero di identificazione per la pubblicità."
  ],

  "what_get_title1": "Cosa Otterrai:",
  "what_get_list1": [
    "Registrazione della licenza turistica sulla Costa del Sol.",
    "Gestione della proprietà con condizioni di affitto favorevoli."
  ],

  "what_get_title2": "Cosa Otterrai:",
  "what_get_list2": [
    "Status legale per pubblicare su portali come Airbnb.",
    "Numero di identificazione per la pubblicità.",
    "Valida per la proprietà, non per il proprietario.",
    "Valida per 6 anni, trasferibile al nuovo proprietario in caso di vendita."
  ],

  "technical_title": "Requisiti Tecnici:",
  "technical_list": [
    "La struttura deve soddisfare gli standard tecnici.",
    "Le variazioni regionali possono influire sui tempi.",
    "Raccolta dei documenti e ottenimento dell'autorizzazione.",
    "Presentazione dei documenti al comune.",
    "Ottenimento di un numero di identificazione.",
    "Consulenza per l'assicurazione della casa."
  ],

  "finaly": "Lascia una richiesta online e ti contatteremo per una consulenza."
}
