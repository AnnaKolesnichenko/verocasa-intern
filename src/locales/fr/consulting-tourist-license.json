{
  "pageTitle": "Licence touristique",
  "pageDescription": "Ce contenu contient des informations en temps réel sur les services juridiques en Espagne",

  "section_description": "Une licence touristique en Espagne est un document essentiel pour ceux qui souhaitent louer leur propriété à des touristes à court terme. La possession d'une telle licence permet non seulement de légaliser l'activité du propriétaire devant la loi, mais aussi d'éviter d'éventuelles amendes en cas de location illégale. Dans ce texte, nous aborderons quatre sujets principaux liés à la licence touristique en Espagne : le processus d'obtention d'une licence, les documents requis, les obligations du propriétaire et les amendes possibles en cas de violation de la loi.",

  "obtaining_title": "Le processus d'obtention d'une licence touristique",
  "obtaining_text": "L'obtention d'une licence touristique en Espagne commence par une demande auprès de l'autorité locale responsable du tourisme dans la région où se trouve le bien. La procédure peut varier d'une communauté autonome à l'autre, car chaque région a ses propres règles et réglementations en matière de location de courte durée. Par exemple, à Barcelone et dans les îles Baléares, les règles peuvent être plus strictes en raison de l'afflux important de touristes.",

  "before_applying": "Avant de déposer une demande, il est important de s'assurer que la propriété répond à toutes les exigences d'une location à court terme. Il faut notamment vérifier que le bâtiment répond aux normes locales de sécurité et de confort. Vous devez ensuite rassembler tous les documents nécessaires et les soumettre à l'autorité compétente. Dans la plupart des cas, la demande est suivie d'une inspection du bien pour s'assurer qu'il répond à toutes les normes. Par exemple, à Barcelone et aux Baléares, les règles peuvent être plus strictes en raison de l'afflux de touristes.",

  "required_title": "Documents requis pour l'obtention d'une licence",
  "required_text": "Un certain nombre de documents sont nécessaires pour obtenir une licence touristique, qui peuvent varier d'une région à l'autre. Toutefois, la liste comprend généralement :",
  "required_list": [
    "Un formulaire de demande de licence rempli conformément aux exigences des autorités locales",
    "Les documents confirmant le titre de propriété",
    "Des plans de construction montrant que le bâtiment est conforme aux codes de sécurité et de prévention des incendies en vigueur",
    "Un certificat de conformité du bien avec les normes locales de confort et de commodité",
    "Dans certains cas, une preuve de l'accord des propriétaires des appartements ou maisons voisins peut être exigée"
  ],

  "lendlors_title": "Responsabilités du propriétaire",
  "lendlors_text": "Après avoir obtenu une licence touristique, un propriétaire doit se conformer à un certain nombre de règles et d'obligations, notamment :",
  "lendlors_list": [
    "Enregistrement de tous les touristes auprès de la police locale ou d'une autre autorité compétente",
    "Le respect des niveaux de bruit et des autres réglementations locales qui peuvent être imposées pour protéger les droits des autres résidents",
    "Maintien de la propriété dans un état conforme aux normes spécifiées dans la licence",
    "Fournir tous les équipements et services annoncés dans l'annonce ou le contrat de location"
  ],

  "posible_title": "Amendes possibles en cas de violation de la législation",
  "posible_text": " Le non-respect des exigences en matière de licence peut entraîner des amendes importantes. Selon la région et la gravité de l'infraction, les amendes peuvent aller de quelques centaines à des milliers d'euros. Les infractions les plus graves comprennent la location de logements sans licence, le fait de ne pas signaler les touristes à la police et la violation des règles de sécurité incendie.",

  "conclusion_title": "Conclusion",
  "conclusion_text": "Une licence touristique en Espagne n'est pas seulement une nécessité légale pour les locations de courte durée, mais aussi un mécanisme permettant de garantir la sécurité et le confort des locataires et des propriétaires eux-mêmes. L'obtention et le respect des conditions de la licence requièrent une attention aux détails et une responsabilité de la part du propriétaire, mais contribuent en fin de compte à maintenir un niveau élevé de services touristiques et à garantir une expérience positive pour les touristes et les locaux...",

  "tourist_license": "Tourist License",

  "important_title": "Moments importants",
  "important_list": [
    "Obligatoire pour les locations de courte durée",
    "Formalisation de 60 à 120 jours.",
    "Numéro d'identification de la publicité."
  ],

  "what_get_title1": "Ce que vous obtiendrez :",
  "what_get_list1": [
    "Enregistrement d'une licence touristique sur la Costa del Sol",
    "Gestion de propriété avec des conditions de location favorables."
  ],

  "what_get_title2": "Ce que vous obtiendrez :",
  "what_get_list2": [
    "Statut légal pour publier sur des portails comme Airbnb",
    "Numéro d'identification de la publicité",
    "Valable pour la propriété, pas pour le propriétaire",
    "Valable 6 ans, passe au nouveau propriétaire lors de la vente."
  ],

  "technical_title": "Exigences techniques :",
  "technical_list": [
    "L'installation doit répondre à des normes techniques",
    "Les variations régionales peuvent avoir une incidence sur le calendrier",
    "Collecte des documents et obtention de l'autorisation",
    "Dépôt des documents auprès de la municipalité",
    "Obtention d'un numéro d'identification.",
    "Conseils en matière d'assurance habitation."
  ],

  "finaly": "Laissez une demande en ligne et nous vous contacterons pour une consultation."
}
