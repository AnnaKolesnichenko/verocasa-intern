import { create } from 'zustand';
import { persist, PersistOptions, createJSONStorage } from 'zustand/middleware';
//імпортував список для тесту, потім буде відбуватися запит на бек прибрати в projects на пустий масив
import { projectsNameOptions } from '@/constants/filters';
export interface PriceType {
  min: number | null;
  max: number | null;
}
export interface ProjectlistItemType {
  id: string;
  name: string;
  coordinates: [number, number];
}

export interface FilterState {
  projects: ProjectlistItemType[];
  mapCoordinates: [number, number];
  mapZoom: number;
  selectedName: string;
  searchParams: string;
  selectedRadio: string;
  selectedType: string[];
  selectedBeds: string[];
  selectedPrice: PriceType;
  selectedSize: PriceType;
  selectedArea: string[];
  polygonData: GeoJSON.Polygon | null;
  reset: boolean;
  setMapObject: (name: string, coordinates: [number, number], zoom: number) => void;
  setFilter: (field: keyof FilterState, value: any) => void;
  toggleReset: () => void;
  clearFilters: () => void;
  setPolygonData: (polygon: GeoJSON.Polygon | null) => void;
  setProjects: (projects: ProjectlistItemType[]) => void;
}

type FilterStateCreator = (
  set: (
    partial: Partial<FilterState> | ((state: FilterState) => Partial<FilterState>),
    replace?: boolean
  ) => void,
  get: () => FilterState,
  api: any
) => FilterState;

const createFilterState: FilterStateCreator = (set, get) => ({
  projects: projectsNameOptions,//[] //дані для компонента search список проектів за іменем
  mapCoordinates: [-4.883333, 36.516666], //координати карти
  mapZoom: 11, //zoom карти
  selectedName: '', //за іменем для map
  searchParams: '', //поле пошуку
  selectedRadio: 'New Building', //radio
  selectedType: [], //apartment, villa ...
  selectedBeds: [], //stydio, 1,2,3
  selectedPrice: { min: null, max: null }, //ціна
  selectedSize: { min: null, max: null }, //розмір
  selectedArea: [], //район
  polygonData: null, //дані полігону//gyhhh
  reset: false,
  setMapObject: (name: string, coordinates: [number, number], zoom: number) =>
    set(state => ({ ...state, selectedName: name, mapCoordinates: coordinates, mapZoom: zoom })),
  setFilter: (field, value) => set(state => ({ ...state, [field]: value })),
  setPolygonData: (polygon: GeoJSON.Polygon | null) =>
    set(state => ({ ...state, polygonData: polygon })),
  toggleReset: () => set(state => ({ reset: !state.reset })),
  clearFilters: () =>
    set({
      mapCoordinates: [-4.883333, 36.516666],
      mapZoom: 11,
      selectedName: '',
      searchParams: '',
      selectedRadio: 'New Building',
      selectedType: [],
      selectedBeds: [],
      selectedPrice: { min: null, max: null },
      selectedSize: { min: null, max: null },
      selectedArea: [],
      polygonData: null,
    }),
  setProjects: (projects: ProjectlistItemType[]) => set({ projects }), //оновлення списку проектів
});

const useFilterStore = create(
  persist<FilterState>(createFilterState, {
    name: 'filter-storage', //ім'я об`єкта
    storage: createJSONStorage(() => sessionStorage),
  } as PersistOptions<FilterState>)
);

export default useFilterStore;
