'use client';

import ResponsiveImage from '@/components/ui/ResponsiveImage';
import React, { useRef } from 'react';
import { Navigation } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import Image from 'next/image';

import 'swiper/css';
import 'swiper/css/navigation';

interface CardSliderProps {
  swiperActive: boolean;
  pictures: string[];
  name: string;
}

const CardSlider = ({ swiperActive, pictures, name }: CardSliderProps) => {
  const sliderRef = useRef<any>(null);
//console.log(pictures)
  // Перевірка чи всі елементи масиву pictures є строками, які починаються з http:// або https://
  const allValidUrls = pictures.every(
    picture =>
      typeof picture === 'string' &&
      (picture.startsWith('http://') || picture.startsWith('https://'))
  );
  // Використовуємо placeholder-изображення, якщо масив pictures не містить тільки строки
  const imagesToDisplay = allValidUrls ? pictures : ['/images/areas/marbella.jpg'];
 // console.log(imagesToDisplay);
  
  return (
    <>
      <Swiper
        style={{ width: '100%', height: '100%' }}
        loop={true}
        onSwiper={swiper => {
          sliderRef.current = swiper;
        }}
        className={'card-swiper'}
        spaceBetween={35}
        slidesPerView={1}
        modules={[Navigation]}
      >
        {imagesToDisplay.map((picture, index) => (
          <SwiperSlide key={index}>
            <ResponsiveImage
              className="card-image"
              src={picture}
              alt={name ? name : 'properties photo'}
              width={588}
              height={388}
            />
          </SwiperSlide>
        ))}
      </Swiper>
      {swiperActive && (
        <>
          <button
            title="previous slide"
            className="swiper-button-prev"
            onClick={e => {
              e.preventDefault();
              sliderRef.current.slidePrev();
            }}
          >
            {arrowL}
          </button>
          <button
            title="next slide"
            className="swiper-button-next"
            onClick={e => {
              e.preventDefault();
              sliderRef.current.slideNext();
            }}
          >
            {arrowR}
          </button>
        </>
      )}
    </>
  );
};

export default CardSlider;

const arrowR = (
  <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23" fill="none">
    <rect width="23" height="23" rx="11.5" transform="matrix(-1 0 0 1 23 0)" fill="white"></rect>
    <path
      d="M10 7L14.1464 11.1464C14.3417 11.3417 14.3417 11.6583 14.1464 11.8536L10 16"
      stroke="black"
      strokeLinecap="round"
    ></path>
  </svg>
);

const arrowL = (
  <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23" fill="none">
    <rect width="23" height="23" rx="11.5" transform="matrix(-1 0 0 1 23 0)" fill="white"></rect>
    <path
      d="M13 7L8.85355 11.1464C8.65829 11.3417 8.65829 11.6583 8.85355 11.8536L13 16"
      stroke="black"
      strokeLinecap="round"
    ></path>
  </svg>
);
