'use client';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import FilterRadioButtons from '@/components/ui/FilterRadiButtons/FilterRadioButtons';
import MapSearch from '@/components/ui/MapSearch/MapSearch';
import { useMediaQuery } from 'react-responsive';
import { useTranslation } from 'react-i18next';
import useFilterStore from '@/store/filterStore';
import useSortStore from '@/store/sortStore';
import { FilterState } from '@/store/filterStore';
import './FilterBlockProperties.scss';
import { PriceType } from '@/store/filterStore';
import PriceChecker from '@/components/ui/PriceChecker/PriceChecker';
import DropdownChecker from '@/components/ui/DropdownChecker/DropdownChecker';
//для запиту отримання списку проектів
import { getProjectsList } from '@/api';
import {
  getTypeOptions,
  getBedroomOptions,
  priceOptions,
  sizeOptions,
  areasOptions,
} from '@/constants/filters';

const FilterBlockProperties = () => {
  const [isClient, setIsClient] = useState<boolean>(false);
  const isMobile = useMediaQuery({ maxWidth: 767 });
  useEffect(() => {
    setIsClient(true);
  }, []);
  const { t } = useTranslation('map-filters');
  const typeOptions = getTypeOptions(t);
  const bedroomOptions = getBedroomOptions(t);

  const {
    projects, //список проектів
    searchParams,
    selectedRadio,
    selectedName,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    reset,
    setMapObject,
    setFilter,
    toggleReset,
    clearFilters,
    setProjects,
  } = useFilterStore();
  const { clearSort } = useSortStore();
  const handleFilterChange = (field: keyof FilterState, value: any) => {
    setFilter(field, value);
  };
  const handleClearFilter = () => {
    clearFilters();
    clearSort();
    toggleReset();
  };
  // якщо проектів не має, тобто вони ще не завантажувались, виконується запит
  useEffect(() => {
    if (projects.length === 0) {
      const fetchData = async () => {
        const newProjects = await getProjectsList();
        setProjects(newProjects);
      };
      fetchData();
    }
    //eslint-disable-next-line
  }, []);
  return (
    isClient &&
    (isMobile ? (
      <div className="filter-block-properties-mob-search container">
        <MapSearch
          handleSearch={value => handleFilterChange('searchParams', value)}
          handleNameChange={setMapObject}
          projectsNameData={projects}
          reset={reset}
        />
      </div>
    ) : (
      <div className="filter-block-properties">
        <div className="filter-block-properties-filters container">
          <div className="row1">
            <div className="radio">
              <FilterRadioButtons
                radio1={t('hero.filter-radio1')}
                radio2={t('hero.filter-radio2')}
                selectedType={selectedRadio}
                action={(value: string) => handleFilterChange('selectedRadio', value)}
              />
            </div>
            <div className="search">
              <MapSearch
                handleSearch={value => handleFilterChange('searchParams', value)}
                handleNameChange={setMapObject}
                projectsNameData={projects}
                reset={reset}
              />
            </div>
            <button onClick={handleClearFilter} className="clear">
              <Image src="/assets/icons/clear.svg" alt="Clear Icon" width={20} height={20} />
              {t('clean')}
            </button>
          </div>

          <div className="row2">
            <div className="dropdown">
              <DropdownChecker
                data={typeOptions}
                title={t('type_title')}
                selectValue={selectedType}
                action={(values: string[]) => handleFilterChange('selectedType', values)}
              />
            </div>

            <div className="dropdown">
              <DropdownChecker
                data={bedroomOptions}
                title={t('beds_title')}
                selectValue={selectedBeds}
                action={(values: string[]) => handleFilterChange('selectedBeds', values)}
              />
            </div>

            <div className="dropdown">
              <PriceChecker
                data={priceOptions}
                title={t('price_title')}
                type="Price"
                selectValue={selectedPrice}
                action={(values: PriceType) => handleFilterChange('selectedPrice', values)}
              />
            </div>

            <div className="dropdown">
              <PriceChecker
                data={sizeOptions}
                title={t('size_title')}
                type="Size"
                selectValue={selectedSize}
                action={(values: PriceType) => handleFilterChange('selectedSize', values)}
              />
            </div>

            <div className="dropdown">
              <DropdownChecker
                data={areasOptions}
                title={t('area_title')}
                selectValue={selectedArea}
                action={(values: string[]) => handleFilterChange('selectedArea', values)}
              />
            </div>
          </div>
        </div>
      </div>
    ))
  );
};

export default FilterBlockProperties;
