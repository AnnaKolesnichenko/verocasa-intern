'use client';
import React from 'react';
import './OffersList.scss';
import Image from 'next/image';
import OfferCard from '../OfferCard';
import { useTranslation } from 'react-i18next';
import CardSkeleton from '../OfferCard/CardSkeleton';
interface OffersListProps {
  offersData: any;
  hideButtons: boolean;
  handleChangePage?: () => void;
  showMessage?: boolean;
  showMore?: boolean;
  isLoading?: boolean;
}

const OffersList = ({
  offersData,
  hideButtons,
  showMessage,
  showMore,
  isLoading,
  handleChangePage,
}: OffersListProps) => {
  const { t } = useTranslation('properties');

  return (
    <div className="offers-list-container">
      {showMessage && <p className="projects__offers-message">{t('offers-list.message')}</p>}
      <div className="projects__offers-list">
        {offersData &&
          Array.isArray(offersData) &&
          offersData.length > 0 &&
          offersData.map((item: any) => {
            return <OfferCard properties={item} key={item.projectId} />;
          })}
        {isLoading && (
          <>
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
          </>
        )}
      </div>
      {!showMore && (
        <p className="projects__offers-message-all-projects">{t('offers-list.message-all')}</p>
      )}
      {!hideButtons && (
        <div className="projects__btn-group">
          <button className="map__btn">
            <div className="circle-mask" />
            <Image src="/assets/icons/earth.svg" alt="Map Icon" width={24} height={24} />
            {t('offers-list.map-btn')}
          </button>
          {showMore && (
            <button onClick={handleChangePage} className="show__btn">
              {t('offers-list.more-btn')}
            </button>
          )}
        </div>
      )}
    </div>
  );
};

export default OffersList;
