import SelectedSwiperSwiper from './SelectedSwiperSwiper';
import { getProjectsMain } from '@/api';
import './SelectedSwiper.scss';
const SelectedSwiper = async() => {
const data = await getProjectsMain();

  return <SelectedSwiperSwiper data={data.content} />;
};
export default SelectedSwiper;

