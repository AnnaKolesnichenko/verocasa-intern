'use client';
import HeroFilterSelect from '../HeroFilterSelect/HeroFilterSelect';
import HeroFilterRadioButtons from '../HeroFilterRadiButtons/HeroFilterRadioButtons';
import { useRouter } from 'next/navigation';
import { useTranslation } from 'react-i18next';
import { areasOptions } from '@/constants/filters';
import useFilterStore from '@/store/filterStore';
import { FilterState } from '@/store/filterStore';
import './HeroFilter.scss';

const HeroFilter = () => {
  const { t } = useTranslation('home');
  
  const router = useRouter();
  const { selectedRadio, selectedArea, setFilter } = useFilterStore();
  const areasData = areasOptions;

  const handleFilterChange = (field: keyof FilterState, value: any) => {
    setFilter(field, value);
  };

  //відправка форми
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    router.push('/properties');
  };

  return (
    <form className="hero-filter" onSubmit={handleSubmit}>
      <HeroFilterSelect
        areasData={areasData}
        selectedAreas={selectedArea}
        action={(values: string[]) => handleFilterChange('selectedArea', values)}
        value={t('hero.filter-select')}
      />
      <HeroFilterRadioButtons
        radio1={t('hero.filter-radio1')}
        radio2={t('hero.filter-radio2')}
        selectedType={selectedRadio}
        action={(values: string) => handleFilterChange('selectedRadio', values)}
      />

      <button className="hero-filter__button opacity" type="submit">
        {t('hero.filter-submit')}
      </button>
      <button className="hero-filter__button-mob opacity" type="submit">
        {t('hero.filter-submit-mob')}
        <span className='icon'>
          <svg
            width="15"
            height="8"
            viewBox="0 0 15 8"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M7.8538 7.8351L14.2835 1.20467C14.6847 0.79094 14.4415 0 13.9131 0H1.0537C0.525289 0 0.282089 0.79094 0.683299 1.20467L7.113 7.8351C7.3261 8.0549 7.6407 8.0549 7.8538 7.8351Z"
              fill="currentColor"
            />
          </svg>
        </span>
      </button>
    </form>
  );
};
export default HeroFilter;
