export type SortModalProps = {
  isOpen: boolean;
  onClose: () => void;
};
