import Image from 'next/image';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import useSortStore from '@/store/sortStore';
import { CustomIconBtn } from '@/components/CustomIconBtn';
import { CustomModal } from '@/components/CustomModal';
import { SortModalProps } from './types';
import './SortModal.scss';

export const SortModal: React.FC<SortModalProps> = ({ isOpen, onClose }) => {
  const { t } = useTranslation('sort-modal');
  const { sortField, sortOrder, setSort} = useSortStore();
  // Локальний стан для тимчасового значення сортування щоб state оновлювався при submit
   // Локальний стан для тимчасового значення сортування
   const [localSortField, setLocalSortField] = useState<string>(sortField);
   const [localSortOrder, setLocalSortOrder] = useState<string>(sortOrder);
 
   const handleSortChange = (sortField: string, sortOrder: string) => {
     setLocalSortField(sortField);
     setLocalSortOrder(sortOrder)
   };

  const handleClearSort = () => {
    setLocalSortField('');
    setLocalSortOrder('');
  };

  const handleSubmitSort = () => {
    setSort('sortField', localSortField);
    setSort('sortOrder', localSortOrder);
    onClose();
  };


  const sort = [
    { label: t('price-asc'), sortField: 'price_from', sortOrder: '' },
    { label: t('price-desc'), sortField: 'price_from', sortOrder: 'desc' },
    { label: t('rating-asc'), sortField: 'raiting_from', sortOrder: '' },
    { label: t('raiting-desc'), sortField: 'raiting_from', sortOrder: 'desc' },
    { label: t('from-to'), sortField: '', sortOrder: '' },
  ];

  return (
    <CustomModal
      isOpen={isOpen}
      classNameModal="modal-sort"
      classNameBackdrop="backdrop-sort"
      onClose={onClose}
    >
      <div className="modal-header__sort">
        <CustomIconBtn className="btn-close__sort" onClick={onClose} ariaLabel="icon close">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
          >
            <path
              d="M7.828 11.0007H20V13.0007L7.828 13.0007L13.192 18.3647L11.778 19.7787L4 12.0007L11.778 4.22266L13.192 5.63666L7.828 11.0007Z"
              fill="#09121F"
            ></path>
          </svg>
        </CustomIconBtn>
        <button onClick={handleClearSort} className="btn-clear__sort">
          {t('clear-sort')}
          <Image src="/assets/icons/clear.svg" alt="Clear Icon" width={20} height={20} />
        </button>
      </div>
      <div className="modal-content__sort">
        {sort.map((option, index) => (
          <label
            key={index}
            className={`modal-content__sort-label ${localSortField === option.sortField && localSortOrder=== option.sortOrder ? 'active' : ''}`}
          >
            <input
              type="radio"
              value={option.sortField}
              checked={localSortField === option.sortField && localSortOrder=== option.sortOrder}
              onChange={() => handleSortChange(option.sortField, option.sortOrder)}
            />
            {option.label}
            <span></span>
          </label>
        ))}
        <button className="modal-content__sort-submit" onClick={handleSubmitSort}>
          {t('btn-apply')}
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            viewBox="0 0 24 24"
            fill="none"
          >
            <path
              d="M12 22C6.477 22 2 17.523 2 12C2 6.477 6.477 2 12 2C17.523 2 22 6.477 22 12C22 17.523 17.523 22 12 22ZM11.003 16L18.073 8.929L16.659 7.515L11.003 13.172L8.174 10.343L6.76 11.757L11.003 16Z"
              fill="white"
            ></path>
          </svg>
        </button>
      </div>
    </CustomModal>
  );
};
