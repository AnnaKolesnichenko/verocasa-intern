'use client';
import './FilterRadioButtons.scss';

interface FilterRadioButtonsProps {
  radio1: string;
  radio2: string;
  selectedType: string;
  action: (value: string) => void;
}

const FilterRadioButtons = ({ selectedType, action, radio1, radio2 }: FilterRadioButtonsProps) => {
  return (
    <div className="filter-radio-buttons">
      <button
        className={`radio-button ${selectedType === 'New Building' ? 'active' : ''}`}
        onClick={event => {
          event.preventDefault();
          action('New Building');
        }}
      >
        {radio1}
      </button>

      <button
        className={`radio-button ${selectedType === 'SpecialOffer' ? 'active' : ''}`}
        onClick={event => {
          event.preventDefault();
          action('SpecialOffer');
        }}
      >
        {radio2}
      </button>
    </div>
  );
};
export default FilterRadioButtons;
