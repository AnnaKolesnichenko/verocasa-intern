import React from 'react';
import ReactModal from 'react-modal';

import { CustomModalProps } from './types';

export const CustomModal: React.FC<CustomModalProps> = ({
  isOpen,
  onClose,
  children,
  classNameModal,
  classNameBackdrop,
}) => {
  return (
    <ReactModal
      isOpen={isOpen}
      overlayClassName={classNameBackdrop}
      className={classNameModal}
      closeTimeoutMS={300}
      onRequestClose={onClose}
      ariaHideApp={false}
    >
      {children}
    </ReactModal>
  );
};
