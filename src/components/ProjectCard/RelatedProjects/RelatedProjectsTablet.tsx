'use client';

import React, { useState } from 'react';
import Image from 'next/image';
import { AnimatePresence, motion } from 'framer-motion';
import Modal from './Modal/Modal';

import { IoIosArrowForward } from 'react-icons/io';

import './relatedProjects.scss';
import { useTranslation } from 'react-i18next';
import { FloorPlan, Plan, Project } from '../MainInfo/types';

interface RelatedProps {
  handleModalOpen: (plan: Plan) => void;
  handleModalClose: () => void;
  projects: Project;
}

const RelatedProjectsTablet: React.FC<RelatedProps> = ({ projects }) => {
  const { t } = useTranslation('project-card');
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedProject, setSelectedProject] = useState<Plan | null>(null);

  const handleModalOpen = (obj: Plan) => {
    if (obj) {
      setSelectedProject(obj);
      setModalOpen(true);
    }
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const params = [
    t('card.related.photo'),
    t('card.related.euro'),
    t('card.related.bed'),
    t('card.related.bathrooms'),
    t('card.related.size'),
    t('card.related.floors'),
    t('card.related.type-floors'),
  ];

  const filledFloorPlanNames = projects.floorPlans.filledFloorPlanNames;
  const floorPlans = projects.floorPlans;

  const matchedFloorPlans = filledFloorPlanNames
    .map(floorKey => {
      const floorPlan = floorPlans[`_${floorKey}` as keyof typeof floorPlans];
      return floorPlan ? floorPlan : null;
    })
    .filter(Boolean) as FloorPlan[];

  return (
    <>
      <p className="projects-title">Property floor plans</p>
      <div className="related-objects-tablet">
        <AnimatePresence>
          {modalOpen && <Modal handleModalClose={handleModalClose} selected={selectedProject} />}
        </AnimatePresence>

        <div>
          <ul>
            {projects.floorPlans.filledFloorPlanNames.map((item, i) => (
              <li key={i}>
                <div className="related-highlight">
                  <p>
                    <span>
                      {item} {t('card.related.bedrooms')}
                    </span>{' '}
                    {t('card.related.bedrooms-from')} {matchedFloorPlans[0].priceFrom} €{' '}
                  </p>
                  <span>
                    {matchedFloorPlans[i].plans.length} {t('card.related.dwellings')}
                  </span>
                </div>
                <div className="projects">
                  <ul className="projects-list">
                    {matchedFloorPlans[i].plans.map((obj, i) => (
                      <li key={i} className="project-item" onClick={() => handleModalOpen(obj)}>
                        <div className="project-item-displayed">
                          <Image
                            src="/images/projectCard/plan.jpg"
                            alt={obj.name}
                            width={226}
                            height={84}
                            className="image"
                          />
                        </div>

                        <div className="project-item-options">
                          <ul className="options-list">
                            <li>
                              <h3>Property Price</h3>
                              <span>{obj.priceFrom}</span>
                            </li>
                            <li>
                              <h3>Total bedrooms</h3>
                              <span>{obj.planInfo.bedrooms}</span>
                            </li>
                            <li>
                              <h3>Total bathrooms</h3>
                              <span>{obj.planInfo.bedrooms}</span>
                            </li>
                            <li>
                              <h3>Living space</h3>
                              <span>{obj.planInfo.buildedSurface}</span>
                            </li>
                            <li>
                              <h3>Total floors</h3>
                              <span>{obj.description.floor}</span>
                            </li>
                            <li>
                              <h3>Property type</h3>
                              <span>{obj.description.type}</span>
                            </li>
                          </ul>

                          <button
                            className="view-info"
                            type="button"
                            onClick={() => handleModalOpen(obj)}
                          >
                            View all information
                            <IoIosArrowForward fill="white" size={24} />
                          </button>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
};

export default RelatedProjectsTablet;
