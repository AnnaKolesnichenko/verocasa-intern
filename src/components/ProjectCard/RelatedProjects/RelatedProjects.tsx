'use client';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Image from 'next/image';
import { AnimatePresence, motion } from 'framer-motion';
import Modal from './Modal/Modal';

import { IoIosArrowForward } from 'react-icons/io';
import { FloorPlan, FloorPlans, Plan, Project } from '../MainInfo/types';
import './relatedProjects.scss';
import RelatedProjectsTablet from './RelatedProjectsTablet';

interface RelatedProps {
  handleModalOpen: (plan: Plan) => void;
  handleModalClose: () => void;
  projects: Project;
}

const RelatedProjects: React.FC<RelatedProps> = ({ projects }) => {
  const { t } = useTranslation('project-card');
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedProject, setSelectedProject] = useState<Plan | null>(null);

  //console.log(selectedProject);

  const handleModalOpen = (obj: Plan) => {
    if (obj) {
      setSelectedProject(obj);
      setModalOpen(true);
    }
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const params = [
    t('card.related.photo'),
    t('card.related.euro'),
    t('card.related.bed'),
    t('card.related.bathrooms'),
    t('card.related.size'),
    t('card.related.floors'),
    t('card.related.type-floors'),
  ];

  const filledFloorPlanNames = projects.floorPlans.filledFloorPlanNames;
  const floorPlans = projects.floorPlans;

  const matchedFloorPlans = filledFloorPlanNames
    .map(floorKey => {
      const floorPlan = floorPlans[`_${floorKey}` as keyof typeof floorPlans];
      return floorPlan ? floorPlan : null;
    })
    .filter(Boolean) as FloorPlan[];

  // console.log(matchedFloorPlans);

  return (
    <>
      <div className="related">
        <AnimatePresence>
          {modalOpen && <Modal handleModalClose={handleModalClose} selected={selectedProject} />}
        </AnimatePresence>
        <div className="features">
          <ul>
            {projects.floorPlans.filledFloorPlanNames.map((item, i) => (
              <li key={i}>
                <div className="related-objects">
                  <p>
                    <span>
                      {item} {t('card.related.bedrooms')}
                    </span>{' '}
                    {t('card.related.bedrooms-from')} {matchedFloorPlans[0].priceFrom} €{' '}
                  </p>
                  <span>
                    {matchedFloorPlans[i].plans.length} {t('card.related.dwellings')}
                  </span>
                </div>
                <ul className="related-features">
                  {params.map((item, i) => (
                    <li key={i}>
                      <span>{item}</span>
                    </li>
                  ))}
                </ul>
                <div className="projects">
                  <ul className="projects-list">
                    {matchedFloorPlans[i].plans.map((obj, i) => {
                      return (
                        <li
                          key={i}
                          className="project-item"
                          onClick={() => {
                            handleModalOpen(obj);
                          }}
                        >
                          <ul className="projects-list-item">
                            <li>
                              {' '}
                              <Image
                                src={obj.image}
                                alt={obj.name}
                                width={226}
                                height={84}
                                className="image"
                              />
                            </li>
                            <li>
                              <span>{obj.priceFrom}</span>
                            </li>
                            <li>
                              {' '}
                              <span>{obj.planInfo.bedrooms}</span>
                            </li>
                            <li>
                              {' '}
                              <span>{obj.planInfo.bathrooms}</span>
                            </li>
                            <li>
                              {' '}
                              <span>{obj.planInfo.buildedSurface}</span>
                            </li>
                            <li>
                              {' '}
                              <span>{obj.description.floor}</span>
                            </li>
                            <li className="arrow-flex-item">
                              {' '}
                              <span>{obj.description.type}</span>
                              <div>
                                <div
                                  style={{
                                    backgroundColor: 'black',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: '34px',
                                    height: '34px',
                                    borderRadius: '100px',
                                  }}
                                >
                                  <motion.div
                                    initial={{ x: 0 }}
                                    whileHover={{
                                      x: 5,
                                      transition: {
                                        duration: 0.5,
                                        type: 'spring',
                                        stiffness: 300,
                                        bounce: 0.7,
                                      },
                                    }}
                                    exit={{ x: 0 }}
                                    style={{
                                      display: 'flex',
                                      justifyContent: 'center',
                                      alignItems: 'center',
                                    }}
                                  >
                                    <IoIosArrowForward fill="white" size={24} />
                                  </motion.div>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </li>
            ))}
          </ul>{' '}
        </div>{' '}
      </div>
      <RelatedProjectsTablet
        projects={projects}
        handleModalClose={handleModalClose}
        handleModalOpen={handleModalOpen}
      />
    </>
  );
};

export default RelatedProjects;
