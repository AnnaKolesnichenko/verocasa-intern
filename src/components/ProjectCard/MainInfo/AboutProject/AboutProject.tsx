'use client';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AnimatePresence, motion } from 'framer-motion';
import ShowArrows from './ShowArrows';

import './aboutProject.scss';

interface Project {
  id: string;
  image: string;
  name: string;
  sizeM2: string;
  priceFrom: string;
  location: string;
  shortDescription: {
    total_floors: string;
    type: string;
    floor: string;
    rooms: string;
    area: string;
  };
  pricePerMeter: string;
  coordinates: {
    lng: string;
    lat: string;
  };
  bed: string;
  bathrooms: string;
  projectFloor: string;

  about: {
    en: {
      title: null;
      description: string;
    };
    ua: null;
    ru: null;
  };
  amenities: {
    id: string;
    image_code: string;
    name: string;
  }[];
  pictures: string[];
}
interface AboutProjectProps {
  item: Project;
}

const AboutProject: React.FC<AboutProjectProps> = ({ item }) => {
  const [fullText, setFullText] = useState(false);
  const { t } = useTranslation('project-card');

  const showFullText = () => {
    setFullText(prevState => !prevState);
  };

  return (
    <div className="about">
      <div className="about-descr">
        <h2 className="about-title">{t('card.description.about-title')}</h2>
        <AnimatePresence>
          {!fullText && (
            <div>
              <div className="sliced-text">
                <p>{item.about.en.description.slice(0, 303)}</p>
                {/* <p>{t('card.description.description.1').slice(0, 303) + '...'}</p> */}
              </div>{' '}
              <ShowArrows fullText={false} showFullText={showFullText} />
            </div>
          )}

          {fullText && (
            <>
              {' '}
              <motion.div
                className="about-list"
                initial={{ y: -50 }}
                animate={{ y: 0 }}
                transition={{ duration: 0.4 }}
                exit={{ y: -50 }}
              >
                <p>{item.about.en.description}</p>
              </motion.div>{' '}
              <ShowArrows fullText={true} showFullText={showFullText} />
            </>
          )}
        </AnimatePresence>
      </div>
      <div className="amenities">
        <h2 className="amenities-title">{t('card.description.amenities')}</h2>
        <ul className="amenities-list">
          {item.amenities.map((amen, i) => (
            <li key={i} className="amenities-list-item">
              <span>{amen.name}</span>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default AboutProject;
