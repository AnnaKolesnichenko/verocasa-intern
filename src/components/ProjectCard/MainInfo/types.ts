export interface Description {
  totalFloors: string;
  type: string;
  floor: number;
  area: number;
}

export interface PlanInfo {
  name: string;
  floor: number;
  bedrooms: number;
  bathrooms: number;
  toilets: number;
  terrace: boolean;
  kitchen: string;
  buildedSurface: number;
  usefulSurface: number;
  terraceSurface: number;
}

export interface Plan {
  image: string;
  name: string;
  location: string;
  description: Description;
  priceFrom: number;
  planInfo: PlanInfo;
}

export interface FloorPlan {
  priceFrom: number;
  plans: Plan[];
}

export interface FloorPlans {
  studio: FloorPlan | null;
  _1: FloorPlan | null;
  _2: FloorPlan | null;
  _3: FloorPlan | null;
  _4: FloorPlan | null;
  _5: FloorPlan | null;
  _6: FloorPlan | null;
  _7: FloorPlan | null;
  filledFloorPlanNames: string[];
}

export interface Project {
  id: string;
  image: string;
  name: string;
  sizeM2: string;
  priceFrom: string;
  location: string;
  short_description: {
    total_floors: string;
    type: string;
    floor: string;
    rooms: string;
    area: string;
  };
  pricePerMeter: string;
  coordinates: {
    lng: string;
    lat: string;
  };
  bed: string;
  bathrooms: string;
  projectFloor: string;

  about: string;
  amenities: {
    id: string;
    image_code: string;
    name: string;
  }[];
  pictures: string[];
  floorPlans: FloorPlans;
}
