'use client';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import MapSearch from '@/components/ui/MapSearch/MapSearch';
import DropdownChecker from '@/components/ui/DropdownChecker/DropdownChecker';
import PriceChecker from '@/components/ui/PriceChecker/PriceChecker';
import useFilterStore, { PriceType, FilterState } from '@/store/filterStore';
import useSortStore from '@/store/sortStore';
import { useTranslation } from 'react-i18next';
import './FilterBlockMap.scss';
//для запиту отримання списку проектів
import { getProjectsList } from '@/api';
import {
  getTypeOptions,
  getBedroomOptions,
  priceOptions,
  sizeOptions,
  areasOptions,
} from '@/constants/filters';

const FilterBlockMap = () => {
  const [isClient, setIsClient] = useState<boolean>(false);
  useEffect(() => {
    setIsClient(true);
  }, []);
  const { t } = useTranslation('map-filters');
  const typeOptions = getTypeOptions(t);
  const bedroomOptions = getBedroomOptions(t);

  const {
    projects, //список проектів
    polygonData,
    searchParams,
    selectedName,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    reset,
    setMapObject,
    setFilter,
    toggleReset,
    clearFilters,
    setProjects,
  } = useFilterStore();
  const { clearSort } = useSortStore();
  const handleFilterChange = (field: keyof FilterState, value: any) => {
    setFilter(field, value);
  };
  const handleClearFilter = () => {
    clearFilters();
    toggleReset();
    clearSort();
  };
  
  // якщо проектів не має, тобто вони ще не завантажувались, виконується запит
  useEffect(() => {
    if (projects.length === 0) {
      const fetchData = async () => {
        const newProjects = await getProjectsList();
        setProjects(newProjects);
      };
      fetchData();
    }
    //eslint-disable-next-line
  }, []);

  return (
    isClient && (
      <div className="filter-block-map container">
        <div className="search">
          <MapSearch
            handleSearch={value => handleFilterChange('searchParams', value)}
            handleNameChange={setMapObject}
            projectsNameData={projects}
            reset={reset}
          />
        </div>
        <div className="dropdown">
          <DropdownChecker
            data={typeOptions}
            title={t('type_title')}
            selectValue={selectedType}
            action={(values: string[]) => handleFilterChange('selectedType', values)}
          />
        </div>

        <div className="dropdown">
          <DropdownChecker
            data={bedroomOptions}
            title={t('beds_title')}
            selectValue={selectedBeds}
            action={(values: string[]) => handleFilterChange('selectedBeds', values)}
          />
        </div>

        <div className="dropdown">
          <PriceChecker
            data={priceOptions}
            title={t('price_title')}
            type="Price"
            selectValue={selectedPrice}
            action={(values: PriceType) => handleFilterChange('selectedPrice', values)}
          />
        </div>

        <div className="dropdown">
          <PriceChecker
            data={sizeOptions}
            title={t('size_title')}
            type="Size"
            selectValue={selectedSize}
            action={(values: PriceType) => handleFilterChange('selectedSize', values)}
          />
        </div>

        <div className="dropdown">
          <DropdownChecker
            data={areasOptions}
            title={t('area_title')}
            selectValue={selectedArea}
            action={(values: string[]) => handleFilterChange('selectedArea', values)}
          />
        </div>

        <button onClick={handleClearFilter} className="clear">
          <Image src="/assets/icons/clear.svg" alt="Clear Icon" width={20} height={20} />
          {t('clean')}
        </button>
      </div>
    )
  );
};

export default FilterBlockMap;
