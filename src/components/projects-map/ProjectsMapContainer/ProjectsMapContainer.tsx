'use client';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import { useMediaQuery } from 'react-responsive';
import FilterBlockMap from '@/components/projects-map/FilterBlockMap/FilterBlockMap';
import ProjectMapMap from '../ProjectsMapMap/ProjectMapMap';
import MapButtons from '../MapButtons/MapButtons';
import useBodyScrollLock from '@/utils/useBodyScrollLock';
import useFilterStore from '@/store/filterStore';
import { useTranslation } from 'react-i18next';
import '../../../app/[locale]/(main)/projects-map/projects-map.scss';
import { getFilteredProjectsToMap } from '@/api/map';
const ProjectsMapContainer = () => {
  const { t } = useTranslation('projects-map');
  const [isClient, setIsClient] = useState<boolean>(false);
  useEffect(() => {
    setIsClient(true);
  }, []);
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [isFullScreenMap, setIsFullScreenMap] = useState(false);
  useBodyScrollLock(isFullScreenMap);
  useBodyScrollLock(isMobile);
  const handleFoolScreenMap = () => {
    setIsFullScreenMap(!isFullScreenMap);
  };
  const {
    searchParams,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    polygonData,
    toggleReset,
    clearFilters,
  } = useFilterStore();

  // Данні для карти
  const [mapData, setMapData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const handleClearFilter = () => {
    clearFilters();
    toggleReset();
  };

  //---------------API----------------
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getFilteredProjectsToMap({
          searchParams,
          selectedType,
          selectedBeds,
          selectedPrice,
          selectedSize,
          selectedArea,
          polygonData,
        });
       // console.log(response.data);
        setMapData(response.data);
        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setMapData([]);
        setIsLoading(false);
      }
    };

    fetchData();
  }, [
    searchParams,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    polygonData,
  ]);
  

  return (
    <section
      className={`map-fool-screen-container ${isFullScreenMap ? 'full-screen' : ''} ${
        isClient && isMobile ? 'full-screen-mobile' : ''
      }`}
    >
      {isClient && !isMobile && (
        <div className="filter-block-map-container">
          <FilterBlockMap />
        </div>
      )}
      <div className="map-container">
        <ProjectMapMap mapData={mapData} isFullScreenMap={isFullScreenMap || isMobile} />
        {isClient && !isMobile && <MapButtons action={handleFoolScreenMap} />}
        {mapData && mapData.length === 0 && !isLoading && (
          <button className="map-container-not-found-message" onClick={handleClearFilter}>
            {t('not-found-message')}
            <Image src="/assets/icons/clear.svg" width={16} height={16} alt="close image" />
          </button>
        )}
      </div>
    </section>
  );
};

export default ProjectsMapContainer;
