'use client';

import MainMapMap from '@/components/mainPage/MainMap8/MainMapMap/MainMapMap';
import React, { useState } from 'react';
import './AreaMap.scss';
import { useTranslation } from 'react-i18next';

type AreaMapProps = {
  mapData: any;
  geographicalInfo: string[];
  lng: string;
  lat: string;
  locale: string;
};

const AreaMap = ({ mapData, geographicalInfo, lat, lng }: AreaMapProps) => {
  const [mapLoad, setLoad] = useState(true);
  const { t } = useTranslation('area-page');
  return (
    <div className="area__map-block">
      <div className="area__location-info">
        <h2 className="section-title">{t('common.map-title')}</h2>
        {geographicalInfo.map((p, idx) => (
          <p key={idx} className="loaction-text">
            {p}
          </p>
        ))}
      </div>
      <div className="area__map">
        {mapLoad && (
          <div className="map-loader">
            <div className="loader"></div>
          </div>
        )}
        <MainMapMap
          mapData={mapData}
          mapCoordinates={[parseFloat(lng), parseFloat(lat)]}
          mapZoom={11}
          setLoad={setLoad}
        />
      </div>
    </div>
  );
};

export default AreaMap;
