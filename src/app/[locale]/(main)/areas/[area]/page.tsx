import './Area.scss';
import OffersList from '@/components/properties-page/OffersList';
import ZoomForm from '@/components/mainPage/ZoomForm7/ZoomForm';
import AreasList from '@/components/properties-page/AreasList';
import { getLocationData } from './data/pageData';
import AreaHead from '@/components/area-page/blocks/head';
import AreaMap from '@/components/area-page/blocks/map';
import AreaAbout from '@/components/area-page/blocks/about';
import AreaRecomendation from '@/components/area-page/blocks/recomendation';
import AreaEntarteiment from '@/components/area-page/blocks/entarteiment';
import initTranslations from '@/app/i18n';
import { getAllProjectsToMapMain, getTopProjects } from '@/api';
interface AreaPageProps {
  params: {
    locale: string;
    area: string;
  };
}

const AreaPage = async ({ params }: AreaPageProps) => {
  const mapData = await getAllProjectsToMapMain();
  const topProjectsData = await getTopProjects();
  const { area, locale } = params;
  const { t } = await initTranslations(locale, ['area-page']);

  const currentArea = getLocationData(t).find(
    (location: { areaName: string }) => location.areaName === area
  );

  if (!currentArea) return null; // make message component

  const {
    areaName,
    title,
    slideImages,
    coords: { lat, lng },
    destination,
    market: { economic, trends },
    geographicalInfo,
    aboutLocation,
    recommendation,
    entertainment,
  } = currentArea;
  // console.log(area);

  return (
    <div className="area container">
      <AreaHead
        areaUrl={area}
        lat={lat}
        lng={lng}
        destination={destination}
        slideImages={slideImages}
        title={title}
      />

      <AreaMap
        mapData={mapData}
        locale={locale}
        geographicalInfo={geographicalInfo}
        lat={lat}
        lng={lng}
      />

      <AreaAbout
        //
        locale={locale}
        aboutLocation={aboutLocation}
        areaName={areaName}
        areaUrl={area}
      />

      <AreaRecomendation
        //
        locale={locale}
        data={recommendation}
      />

      <AreaEntarteiment
        //
        data={entertainment}
        areaName={areaName}
      />

      <div className="area__properties">
        <h2 className="section-title--projects">
          {t('common.projects-title')} {title}
        </h2>
        <OffersList hideButtons={true} offersData={topProjectsData} />
      </div>

      <div className="area__zoom-form">
        <ZoomForm />
      </div>

      <div className="area__market">
        <div className="economic">
          <h3>{t('common.economic')}</h3>
          <p className="text">{economic}</p>
        </div>
        <div className="trends">
          <h3>{t('common.trends')}</h3>
          <p className="text">{trends}</p>
        </div>
      </div>

      <div className="area__other-areas">
        <AreasList allAreas={false} title={t('common.other-areas')} />
      </div>
    </div>
  );
};

export default AreaPage;
