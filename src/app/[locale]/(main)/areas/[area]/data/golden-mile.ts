const golSlide1 = '/assets/images/area/golden-mile/Slide1.jpg';
const golSlide2 = '/assets/images/area/golden-mile/Slide2.jpg';
const golSlide3 = '/assets/images/area/golden-mile/Slide3.jpg';
const golSlide4 = '/assets/images/area/golden-mile/Slide4.jpeg';
// const golSlide5 = '/assets/images/area/golden-mile/Slide1.jpg';

const golRec1 = '/assets/images/area/golden-mile/rec1.jpg';
const golRec2 = '/assets/images/area/golden-mile/rec2.jpg';
const golRec3 = '/assets/images/area/golden-mile/rec3.jpg';
const golRec4 = '/assets/images/area/golden-mile/rec4.jpg';

const golBeach1 = '/assets/images/area/golden-mile/bch1.webp';
const golBeach2 = '/assets/images/area/golden-mile/bch2.jpg';
const golBeach3 = '/assets/images/area/golden-mile/bch3.jpg';
const golBeach4 = '/assets/images/area/golden-mile/bch4.jpg';

const MainMijas = '/assets/images/area/mijas/MainMijas.png';
const MainMijass = '/assets/images/area/marbella/MainMarbella.png';

export const getGoldenMileData = (t: (arg0: string) => any) => {
  return {
    areaName: 'golden-mile',
    title: t('golden-mile.title'),
    coords: { lat: '36.5164', lng: '-4.8946' },
    prices: ['€500K', '€1M', '€1.2M'],
    slideImages: [
      golSlide1,
      golSlide2,
      golSlide3,
      golSlide4,
      // golSlide5
    ],
    destination: {
      plane: [
        {
          time: '2,55',
          city: t('golden-mile.destination.plane.london'),
        },
        {
          time: '10,55',
          city: t('golden-mile.destination.plane.dubai'),
        },
        {
          time: '9,35',
          city: t('golden-mile.destination.plane.new-york'),
        },
        {
          time: '1,15',
          city: t('golden-mile.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '50',
          city: t('golden-mile.destination.car.malaga'),
        },
        {
          time: '2,40',
          city: t('golden-mile.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('golden-mile.geographicalInfo.0'),
      t('golden-mile.geographicalInfo.1'),
      t('golden-mile.geographicalInfo.2'),
      t('golden-mile.geographicalInfo.3'),
      t('golden-mile.geographicalInfo.4'),
    ],
    aboutLocation: {
      gallery: [MainMijas, MainMijass],
      paragraphs: [
        t('golden-mile.aboutLocation.paragraphs.0'),
        t('golden-mile.aboutLocation.paragraphs.1'),
        t('golden-mile.aboutLocation.paragraphs.2'),
        t('golden-mile.aboutLocation.paragraphs.3'),
        t('golden-mile.aboutLocation.paragraphs.4'),
      ],
    },
    recommendation: [
      {
        title: t('golden-mile.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: golRec1,
      },
      {
        title: t('golden-mile.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: golRec2,
      },
      {
        title: t('golden-mile.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: golRec3,
      },
      {
        title: t('golden-mile.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: golRec4,
      },
    ],
    entertainment: [
      {
        title: t('golden-mile.entertainment.0.title'),
        photo: golBeach1,
        modalText: t('golden-mile.entertainment.0.modalText'),
        modalTitle: t('golden-mile.entertainment.0.modalTitle'),
      },
      {
        title: t('golden-mile.entertainment.1.title'),
        photo: golBeach2,
        modalText: t('golden-mile.entertainment.1.modalText'),
        modalTitle: t('golden-mile.entertainment.1.modalTitle'),
      },
      {
        title: t('golden-mile.entertainment.2.title'),
        photo: golBeach3,
        modalText: t('golden-mile.entertainment.2.modalText'),
        modalTitle: t('golden-mile.entertainment.2.modalTitle'),
      },
      {
        title: t('golden-mile.entertainment.3.title'),
        photo: golBeach4,
        modalText: t('golden-mile.entertainment.3.modalText'),
        modalTitle: t('golden-mile.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('golden-mile.market.economic'),
      trends: t('golden-mile.market.trends'),
    },
  };
};
