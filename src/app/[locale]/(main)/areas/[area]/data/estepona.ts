const rec1 = '/assets/images/area/estapona/recomend1.png';
const rec2 = '/assets/images/area/estapona/recomend2.png';
const rec3 = '/assets/images/area/estapona/recomend3.png';
const rec4 = '/assets/images/area/estapona/recomend4.png';

const beachBar4 = '/assets/images/area/estapona/beach4.png';
const beachBar7 = '/assets/images/area/estapona/beach7.png';
const beachBar8 = '/assets/images/area/estapona/beach8.png';
const beachBar9 = '/assets/images/area/estapona/beach9.png';

const estSlide1 = '/assets/images/area/estapona/slide1.png';
const estSlide2 = '/assets/images/area/estapona/slide2.png';
const estSlide3 = '/assets/images/area/estapona/slide3.png';
const estSlide4 = '/assets/images/area/estapona/slide4.jpeg';

const MainEstapona = '/assets/images/area/estapona/MaiinEstapona.png';
const MainEstaponas = '/assets/images/area/estapona/MaiinEstaponas.png';

export const getEsteponaData = (t: (arg0: string) => any) => {
  return {
    areaName: 'estepona',
    coords: { lat: '36.42764', lng: '-5.14589' },
    title: t('estepona.title'),
    prices: ['€500K', '€1.2M', '€1.5M', '€1.7M', '€2M'],
    slideImages: [estSlide1, estSlide2, estSlide3, estSlide4, estSlide3],
    destination: {
      plane: [
        {
          time: '2,55',
          city: t('estepona.destination.plane.london'),
        },
        {
          time: '10,55',
          city: t('estepona.destination.plane.dubai'),
        },
        {
          time: '9,35',
          city: t('estepona.destination.plane.newYork'),
        },
        {
          time: '1,15',
          city: t('estepona.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '55',
          city: t('estepona.destination.car.malaga'),
        },
        {
          time: '2,40',
          city: t('estepona.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('estepona.geographicalInfo.0'),
      t('estepona.geographicalInfo.1'),
      t('estepona.geographicalInfo.2'),
      t('estepona.geographicalInfo.3'),
      t('estepona.geographicalInfo.4'),
    ],
    aboutLocation: {
      gallery: [MainEstapona, MainEstaponas],
      paragraphs: [
        t('estepona.aboutLocation.paragraphs.0'),
        t('estepona.aboutLocation.paragraphs.1'),
        t('estepona.aboutLocation.paragraphs.2'),
        t('estepona.aboutLocation.paragraphs.3'),
        t('estepona.aboutLocation.paragraphs.4'),
      ],
    },
    recommendation: [
      {
        title: t('estepona.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: rec1,
      },
      {
        title: t('estepona.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: rec2,
      },
      {
        title: t('estepona.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: rec3,
      },
      {
        title: t('estepona.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: rec4,
      },
    ],
    entertainment: [
      {
        title: t('estepona.entertainment.0.title'),
        photo: beachBar8,
        modalText: t('estepona.entertainment.0.modalText'),
        modalTitle: t('estepona.entertainment.0.modalTitle'),
      },
      {
        title: t('estepona.entertainment.1.title'),
        photo: beachBar9,
        modalText: t('estepona.entertainment.1.modalText'),
        modalTitle: t('estepona.entertainment.1.modalTitle'),
      },
      {
        title: t('estepona.entertainment.2.title'),
        photo: beachBar7,
        modalText: t('estepona.entertainment.2.modalText'),
        modalTitle: t('estepona.entertainment.2.modalTitle'),
      },
      {
        title: t('estepona.entertainment.3.title'),
        photo: beachBar4,
        modalText: t('estepona.entertainment.3.modalText'),
        modalTitle: t('estepona.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('estepona.market.economic'),
      trends: t('estepona.market.trends'),
    },
  };
};
