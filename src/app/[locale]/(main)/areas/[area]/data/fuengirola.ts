const MainFuengirolas = '/assets/images/area/fuengirola/MainFuengirolas.png';
const MainFuengirola = '/assets/images/area/fuengirola/MainFuengirola.png';

const fueSlide1 = '/assets/images/area/fuengirola/slide1.png';
const fueSlide2 = '/assets/images/area/fuengirola/slide2.png';
const fueSlide3 = '/assets/images/area/fuengirola/slide3.png';
const fueSlide4 = '/assets/images/area/fuengirola/slide4.png';

const rec1 = '/assets/images/area/fuengirola/recomend1.png';
const rec2 = '/assets/images/area/fuengirola/recomend2.png';
const rec3 = '/assets/images/area/fuengirola/recomend3.png';
const rec4 = '/assets/images/area/fuengirola/recomend4.png';

const beachBar4 = '/assets/images/area/fuengirola/beach4.png';
const beachBar10 = '/assets/images/area/fuengirola/beach10.png';
const beachBar11 = '/assets/images/area/fuengirola/beach11.png';
const beachBar12 = '/assets/images/area/fuengirola/12.png';

export const getFuengirolaData = (t: (arg0: string) => any) => {
  return {
    areaName: 'fuengirola',
    title: t('fuengirola.title'),
    coords: { lat: '36.520489', lng: '-5.045557' },
    prices: ['€500K', '€1M', '€1.7M', '€2.4M'],
    slideImages: [fueSlide1, fueSlide2, fueSlide3, fueSlide4],
    destination: {
      plane: [
        {
          time: '2,55',
          city: t('fuengirola.destination.plane.london'),
        },
        {
          time: '10,55',
          city: t('fuengirola.destination.plane.dubai'),
        },
        {
          time: '9,35',
          city: t('fuengirola.destination.plane.new-york'),
        },
        {
          time: '1,15',
          city: t('fuengirola.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '50',
          city: t('fuengirola.destination.car.malaga'),
        },
        {
          time: '2,40',
          city: t('fuengirola.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('fuengirola.geographicalInfo.0'),
      t('fuengirola.geographicalInfo.1'),
      t('fuengirola.geographicalInfo.2'),
      t('fuengirola.geographicalInfo.3'),
      t('fuengirola.geographicalInfo.4'),
    ],
    aboutLocation: {
      gallery: [MainFuengirola, MainFuengirolas],
      paragraphs: [
        t('fuengirola.aboutLocation.paragraphs.0'),
        t('fuengirola.aboutLocation.paragraphs.1'),
        t('fuengirola.aboutLocation.paragraphs.2'),
        t('fuengirola.aboutLocation.paragraphs.3'),
        t('fuengirola.aboutLocation.paragraphs.4'),
      ],
    },
    recommendation: [
      {
        title: t('fuengirola.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: rec1,
      },
      {
        title: t('fuengirola.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: rec2,
      },
      {
        title: t('fuengirola.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: rec3,
      },
      {
        title: t('fuengirola.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: rec4,
      },
    ],
    entertainment: [
      {
        title: t('fuengirola.entertainment.0.title'),
        photo: beachBar11,
        modalText: t('fuengirola.entertainment.0.modalText'),
        modalTitle: t('fuengirola.entertainment.0.modalTitle'),
      },
      {
        title: t('fuengirola.entertainment.1.title'),
        photo: beachBar12,
        modalText: t('fuengirola.entertainment.1.modalText'),
        modalTitle: t('fuengirola.entertainment.1.modalTitle'),
      },
      {
        title: t('fuengirola.entertainment.2.title'),
        photo: beachBar10,
        modalText: t('fuengirola.entertainment.2.modalText'),
        modalTitle: t('fuengirola.entertainment.2.modalTitle'),
      },
      {
        title: t('fuengirola.entertainment.3.title'),
        photo: beachBar4,
        modalText: t('fuengirola.entertainment.3.modalText'),
        modalTitle: t('fuengirola.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('fuengirola.market.economic'),
      trends: t('fuengirola.market.trends'),
    },
  };
};
