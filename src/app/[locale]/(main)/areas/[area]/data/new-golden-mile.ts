const nueRec1 = '/assets/images/area/new-golden-mile/rec1.jpg';
const nueRec2 = '/assets/images/area/new-golden-mile/rec2.jpg';
const nueRec3 = '/assets/images/area/new-golden-mile/rec3.jpg';
const nueRec4 = '/assets/images/area/new-golden-mile/rec4.jpg';

const nuebch1 = '/assets/images/area/new-golden-mile/bch1.jpg';
const nuebch2 = '/assets/images/area/new-golden-mile/bch2.jpg';
const nuebch3 = '/assets/images/area/new-golden-mile/bch3.jpg';
const nuebch4 = '/assets/images/area/new-golden-mile/bch4.jpg';

const nueSlide1 = '/assets/images/area/new-golden-mile/newSlide1.jpeg';
const nueSlide2 = '/assets/images/area/new-golden-mile/newSlide2.jpg';
const nueSlide3 = '/assets/images/area/new-golden-mile/newSlide3.jpg';
const nueSlide4 = '/assets/images/area/new-golden-mile/newSlide4.jpg';
const nueSlide5 = '/assets/images/area/new-golden-mile/newSlide5.jpg';
const nueSlide6 = '/assets/images/area/new-golden-mile/newSlide6.jpg';
const nueSlide7 = '/assets/images/area/new-golden-mile/newSlide7.jpg';
const nueSlide8 = '/assets/images/area/new-golden-mile/newSlide8.jpg';

const MainMarbella = '/assets/images/area/marbella/MainMarbellas.png';
const MainMarbellas = '/assets/images/area/marbella/MainMarbella.png';

export const getNewGoldenMileData = (t: (arg0: string) => any) => {
  return {
    areaName: 'new-golden-mile',
    title: t('new-golden-mile.title'),
    coords: { lat: '36.4500', lng: '-5.0000' },
    prices: ['€500K', '€1M', '€1.2M'],
    slideImages: [
      nueSlide1,
      nueSlide2,
      nueSlide3,
      nueSlide4,
      nueSlide5,
      nueSlide6,
      nueSlide7,
      nueSlide8,
    ],
    destination: {
      plane: [
        {
          time: '2,55',
          city: t('new-golden-mile.destination.plane.london'),
        },
        {
          time: '10,55',
          city: t('new-golden-mile.destination.plane.dubai'),
        },
        {
          time: '9,35',
          city: t('new-golden-mile.destination.plane.new-york'),
        },
        {
          time: '1,15',
          city: t('new-golden-mile.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '50',
          city: t('new-golden-mile.destination.car.malaga'),
        },
        {
          time: '2,40',
          city: t('new-golden-mile.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('new-golden-mile.geographicalInfo.0'),
      t('new-golden-mile.geographicalInfo.1'),
      t('new-golden-mile.geographicalInfo.2'),
      t('new-golden-mile.geographicalInfo.3'),
    ],
    aboutLocation: {
      gallery: [MainMarbellas, MainMarbella],
      paragraphs: [
        t('new-golden-mile.aboutLocation.paragraphs.0'),
        t('new-golden-mile.aboutLocation.paragraphs.1'),
        t('new-golden-mile.aboutLocation.paragraphs.2'),
        t('new-golden-mile.aboutLocation.paragraphs.3'),
        t('new-golden-mile.aboutLocation.paragraphs.4'),
        t('new-golden-mile.aboutLocation.paragraphs.5'),
      ],
    },
    recommendation: [
      {
        title: t('new-golden-mile.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: nueRec1,
      },
      {
        title: t('new-golden-mile.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: nueRec2,
      },
      {
        title: t('new-golden-mile.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: nueRec3,
      },
      {
        title: t('new-golden-mile.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: nueRec4,
      },
    ],
    entertainment: [
      {
        title: t('new-golden-mile.entertainment.0.title'),
        photo: nuebch1,
        modalText: t('new-golden-mile.entertainment.0.modalText'),
        modalTitle: t('new-golden-mile.entertainment.0.modalTitle'),
      },
      {
        title: t('new-golden-mile.entertainment.1.title'),
        photo: nuebch2,
        modalText: t('new-golden-mile.entertainment.1.modalText'),
        modalTitle: t('new-golden-mile.entertainment.1.modalTitle'),
      },
      {
        title: t('new-golden-mile.entertainment.2.title'),
        photo: nuebch3,
        modalText: t('new-golden-mile.entertainment.2.modalText'),
        modalTitle: t('new-golden-mile.entertainment.2.modalTitle'),
      },
      {
        title: t('new-golden-mile.entertainment.3.title'),
        photo: nuebch4,
        modalText: t('new-golden-mile.entertainment.3.modalText'),
        modalTitle: t('new-golden-mile.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('new-golden-mile.market.economic'),
      trends: t('new-golden-mile.market.trends'),
    },
  };
};
