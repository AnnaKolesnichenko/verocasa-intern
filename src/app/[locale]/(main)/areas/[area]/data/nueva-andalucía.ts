const nueSlide1 = '/assets/images/area/nueva/nueSlide1.jpg';
const nueSlide2 = '/assets/images/area/nueva/nueSlide2.webp';
const nueSlide3 = '/assets/images/area/nueva/nueSlide3.jpeg';
const nueSlide4 = '/assets/images/area/nueva/nueSlide4.jpg';
const nueSlide5 = '/assets/images/area/nueva/nueSlide5.jpg';

const nueRec1 = '/assets/images/area/nueva/nueRec1.jpg';
const nueRec2 = '/assets/images/area/nueva/nueRec2.webp';
const nueRec3 = '/assets/images/area/nueva/nueRec3.jpg';
const nueRec4 = '/assets/images/area/nueva/nueRec4.jpg';

const nuebch1 = '/assets/images/area/nueva/bch1.jpg';
const nuebch2 = '/assets/images/area/nueva/bch2.webp';
const nuebch3 = '/assets/images/area/nueva/nuebch3.webp';
const nuebch4 = '/assets/images/area/nueva/bch4.jpg';

const MainMijas = '/assets/images/area/mijas/MainMijas.png';
const MainMijass = '/assets/images/area/fuengirola/MainFuengirolas.png';

export const getNuevaData = (t: (arg0: string) => any) => {
  return {
    areaName: 'nueva-andalucia',
    title: t('nueva-andalucía.title'),
    coords: { lat: '36.51543', lng: '-4.88583' },
    prices: ['€500K', '€1M', '€1.2M'],
    slideImages: [nueSlide1, nueSlide2, nueSlide3, nueSlide4, nueSlide5],
    destination: {
      plane: [
        {
          time: '2,50',
          city: t('nueva-andalucía.destination.plane.london'),
        },
        {
          time: '12,00',
          city: t('nueva-andalucía.destination.plane.dubai'),
        },
        {
          time: '9,15',
          city: t('nueva-andalucía.destination.plane.newYork'),
        },
        {
          time: '1,20',
          city: t('nueva-andalucía.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '45',
          city: t('nueva-andalucía.destination.car.malaga'),
        },
        {
          time: '2,20',
          city: t('nueva-andalucía.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('nueva-andalucía.geographicalInfo.0'),
      t('nueva-andalucía.geographicalInfo.1'),
      t('nueva-andalucía.geographicalInfo.2'),
      t('nueva-andalucía.geographicalInfo.3'),
    ],
    aboutLocation: {
      gallery: [MainMijas, MainMijass],
      paragraphs: [
        t('nueva-andalucía.aboutLocation.paragraphs.0'),
        t('nueva-andalucía.aboutLocation.paragraphs.1'),
        t('nueva-andalucía.aboutLocation.paragraphs.2'),
        t('nueva-andalucía.aboutLocation.paragraphs.3'),
        t('nueva-andalucía.aboutLocation.paragraphs.4'),
      ],
    },
    recommendation: [
      {
        title: t('nueva-andalucía.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: nueRec1,
      },
      {
        title: t('nueva-andalucía.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: nueRec2,
      },
      {
        title: t('nueva-andalucía.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: nueRec3,
      },
      {
        title: t('nueva-andalucía.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: nueRec4,
      },
    ],
    entertainment: [
      {
        title: t('nueva-andalucía.entertainment.0.title'),
        photo: nuebch1,
        modalText: t('nueva-andalucía.entertainment.0.modalText'),
        modalTitle: t('nueva-andalucía.entertainment.0.modalTitle'),
      },
      {
        title: t('nueva-andalucía.entertainment.1.title'),
        photo: nuebch2,
        modalText: t('nueva-andalucía.entertainment.1.modalText'),
        modalTitle: t('nueva-andalucía.entertainment.1.modalTitle'),
      },
      {
        title: t('nueva-andalucía.entertainment.2.title'),
        photo: nuebch3,
        modalText: t('nueva-andalucía.entertainment.2.modalText'),
        modalTitle: t('nueva-andalucía.entertainment.2.modalTitle'),
      },
      {
        title: t('nueva-andalucía.entertainment.3.title'),
        photo: nuebch4,
        modalText: t('nueva-andalucía.entertainment.3.modalText'),
        modalTitle: t('nueva-andalucía.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('nueva-andalucía.market.economic'),
      trends: t('nueva-andalucía.market.trends'),
    },
  };
};
