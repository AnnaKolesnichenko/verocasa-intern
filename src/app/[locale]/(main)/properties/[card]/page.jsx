'use client';

import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { useTranslation } from 'react-i18next';
import { IoIosArrowForward } from 'react-icons/io';
import { projectCardItems } from '../../../../../components/ProjectCard/cardDemo';
import MainInfo from '../../../../../components/ProjectCard/MainInfo/MainInfo/MainInfo';
import AboutProject from '../../../../../components/ProjectCard/MainInfo/AboutProject/AboutProject';
import RelatedProjects from '../../../../../components/ProjectCard/RelatedProjects/RelatedProjects';
import LegalServices from '../../../../../components/ProjectCard/LegalServices/LegalServices';
import OtherProjects from '../../../../../components/ProjectCard/OtherProjects/OtherProjects';
import PropertyMap from '@/components/ProjectCard/PropertyMap/PropertyMap';

import '../../../../../styles/global.scss';
import '../../../../../components/ProjectCard/MainInfo/MainInfo/mainInfo.scss';
import { getProjectById, getProjectsList } from '@/api';

const Card = ({ params }) => {
  const { t } = useTranslation('project-card');
  const { card } = params;

  const [project, setProject] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchProject = async () => {
      try {
        const data = await getProjectById(card);
        //console.log(data)
        setProject(data);
      } catch (err) {
        console.error(err);
      } finally {
        setLoading(false);
      }
    };

    fetchProject();
  }, [card]);

  const [showAdditionalMessage, setShowAdditionalMessage] = useState(false);

  useEffect(() => {
    let timer;
    if (loading) {
      timer = setTimeout(() => {
        setShowAdditionalMessage(true);
      }, 2000);
    } else {
      setShowAdditionalMessage(false);
    }
    return () => clearTimeout(timer);
  }, [loading]);

  if (loading) {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: '20px',
          alignItems: 'center',
          justifyContent: 'center',
          padding: '30px',
          fontSize: '30px',
        }}
      >
        <p style={{ color: 'coral' }}>Loading.... wait please</p>
        {showAdditionalMessage && (
          <p style={{ color: '#305131' }}>Do not give up...we are searching for your project</p>
        )}
      </div>
    );
  }
  if (error) return <p>Error loading project: {error.message}</p>;
  if (!project) return <p>There is no project found</p>;

  return (
    <div className="container" style={{ paddingTop: '22px' }}>
      <div className="breadcrumbs">
        <Link href="/">{t('card.link-home')}</Link>
        <IoIosArrowForward
          width="6px"
          height="10px"
          color="#676767"
          style={{ display: 'flex', alignSelf: 'center', justifyContent: 'center' }}
        />
        <Link href="/properties">{t('card.link-properties')}</Link>
        <IoIosArrowForward
          width="6px"
          height="10px"
          color="#676767"
          style={{ display: 'flex', alignSelf: 'center', justifyContent: 'center' }}
        />
        <span>{project.name}</span>
      </div>
      <MainInfo item={project} />
      <AboutProject item={project} />
      <PropertyMap lat={project.coordinates.lat} lng={project.coordinates.lng} />
      <RelatedProjects projects={project} />
      <LegalServices />
      <OtherProjects projects={projectCardItems} />
    </div>
  );
};

export default Card;
