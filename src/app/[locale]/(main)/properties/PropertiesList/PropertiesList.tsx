'use client';
import { useState, useEffect } from 'react';
import OffersList from '@/components/properties-page/OffersList';
import { getFilteredProjectsToProperties } from '@/api/properties';
import useFilterStore from '@/store/filterStore';
import useSortStore from '@/store/sortStore';

const PropertiesList = () => {
  const [propertiesData, setPropertiesData] = useState<any>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [showMessage, setShowMessage] = useState<boolean>(false);
  const [page, setPage] = useState<number>(1);
  const [pageCount, setPageCount] = useState<number>(2);
  const { sortField, sortOrder } = useSortStore();
  const {
    selectedRadio,
    searchParams,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
  } = useFilterStore();

  const handleChangePage = () => {
    if (page < pageCount) {
      setPage(prev => prev + 1);
    }
  };
  //запит
  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await getFilteredProjectsToProperties({
          sortField,
          sortOrder,
          selectedRadio,
          searchParams,
          selectedType,
          selectedBeds,
          selectedPrice,
          selectedSize,
          selectedArea,
          page: 1,
        });

        setPropertiesData(response.content);
        setPageCount(response.totalPages);
        setPage(1);
        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    };

    fetchData();
  }, [
    sortField,
    sortOrder,
    selectedRadio,
    searchParams,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
  ]);

  useEffect(() => {
    if (page === 1) return;

    const fetchData = async () => {
      try {
        setIsLoading(true);
        const response = await getFilteredProjectsToProperties({
          sortField,
          sortOrder,
          selectedRadio,
          searchParams,
          selectedType,
          selectedBeds,
          selectedPrice,
          selectedSize,
          selectedArea,
          page,
        });
        setPropertiesData((prevData: any) => [...prevData, ...response.content]);
        setPageCount(response.totalPages);
        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    };

    fetchData();
    // eslint-disable-next-line
  }, [page]);

  useEffect(() => {
    const filtersApplied = Boolean(
      selectedRadio !== 'New Building' ||
        searchParams ||
        (selectedType && selectedType.length > 0) ||
        (selectedBeds && selectedBeds.length > 0) ||
        (selectedPrice && (selectedPrice.min !== null || selectedPrice.max !== null)) ||
        (selectedSize && (selectedSize.min !== null || selectedSize.max !== null)) ||
        (selectedArea && selectedArea.length > 0)
    );

    setShowMessage(filtersApplied);
  }, [
    sortField,
    sortOrder,
    selectedRadio,
    searchParams,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
  ]);

  return (
    <OffersList
      hideButtons={false}
      showMessage={showMessage && propertiesData && propertiesData.length > 0} //&& propertiesData && propertiesData.length > 0
      showMore={page < pageCount}
      offersData={propertiesData}
      handleChangePage={handleChangePage}
      isLoading={isLoading}
    />
  );
};

export default PropertiesList;
