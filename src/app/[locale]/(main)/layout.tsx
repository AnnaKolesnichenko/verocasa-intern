import type { Metadata } from 'next';
import { ReactNode } from 'react';

import { Footer } from '@/layout/Footer';
import { Header } from '@/layout/Header';

export const metadata: Metadata = {
  title: 'Property in Costa de Sol, Spain | Property Partners Spain',
  description: 'Property in Costa de Sol, Spain |Real estate company Spain | Property Partners Spain',
};
interface LayoutProps {
  children: ReactNode;
  params: { locale: string };
}

export default async function RootLayout({ children, params: { locale } }: LayoutProps) {
  return (
    <>
      <Header />
      <main>{children}</main>
      <Footer />
    </>
  );
}
