module.exports = {
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'img.pro-part.es',
        pathname: '/**',
      },
      {
        protocol: 'https',
        hostname: 'newbuildingspain.com',
        pathname: '/media/photo/**',
      },
      {
        protocol: 'https',
        hostname: 'cdn.resales-online.com',
        pathname: '/public/**',
      },
    ],
  },
};
